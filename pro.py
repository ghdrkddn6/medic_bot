# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from urllib import parse

from slack.web.classes import extract_json
from slack.web.classes.blocks import *


SLACK_TOKEN = "xoxb-691907654806-689765112912-UwywGmWM6VssIpELqpTxhjDb"
SLACK_SIGNING_SECRET = "8d6e0c04bf0af611ac4dcbf2771fdc57"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

s_word = ""
# 크롤링 함수 구현하기
def _crawl_medicine_name_list(text):
    with open("input.txt", "w") as f:
        f.write(text)

    url = "https://terms.naver.com/medicineSearch.nhn?mode=nameSearch&query=" + parse.quote(text)

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    list_name = []
    for i in soup.find('ul', class_='content_list').find_all('strong', class_='title'):
        list_name.append(i.find('strong').get_text().strip())

    chat_text_list = []
    chat_text_list.append("*'" + text + "' 검색결과*")
    for i in range(len(list_name)):
        chat_text_list.append(str(i+1) + ". " + list_name[i] + "\n")

    return u'\n'.join(chat_text_list)

# 크롤링 함수 구현하기
def _crawl_medicine_body_list(sel_num):
    with open("input.txt", "r") as f:
        s_word=f.read()

    url = "https://terms.naver.com/medicineSearch.nhn?mode=nameSearch&query=" + parse.quote(s_word)

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    list_name = []
    list_href = []
    for i in soup.find('ul', class_='content_list').find_all('strong', class_='title'):
        list_name.append(i.find('strong').get_text().strip())
        list_href.append("https://terms.naver.com" + i.find('a')['href'])

    chat_text_list = []
    for i in range(len(list_name)):
        if i == int(sel_num)-1:
            chat_text_list.append("*"+str(i+1) + ". " + list_name[i] + "*\n")

        list_medicine = []
        source_code2 = urllib.request.urlopen(list_href[i]).read()
        soup2 = BeautifulSoup(source_code2, "html.parser")
        for j in soup2.find('div', class_='size_ct_v2').find_all('p', class_='txt'):
            list_medicine.append(j.get_text().strip() + "\n")

        if i == int(sel_num)-1:
            chat_text_list.append("*효능효과*")
            chat_text_list.append(list_medicine[3] + "\n")
            chat_text_list.append("*성분정보*")
            chat_text_list.append(list_medicine[1] + "\n")
            chat_text_list.append("*<" + list_href[i]+"|자세한 정보는 여기를 클릭하세요.>*" + "\n")

    return u'\n'.join(chat_text_list)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    input1 = text.split()[1]
    if input1 == "검색":
        s_word = text.split()[2]
        keywords = _crawl_medicine_name_list(s_word)
    elif input1 == "선택":
        s_word = text.split()[2]
        keywords = _crawl_medicine_body_list(s_word)
    elif input1.lower() == "help":
        keywords = "*[사용법]*\n<@챗봇> [AAA] [BBB]\nAAA: '검색' or '선택' 입력\nBBB: AAA가 '검색'일경우 검색할 약이름 입력\nBBB: AAA가 '선택'일경우 선택할 숫자 입력\n\n"
        keywords += "*[사용법 예시]*\n<@챗봇> 검색 타이레놀\n<@챗봇> 선택 1\n"
    else:
        keywords = "잘못된 입력입니다.\n도움말 '<@챗봇> help'을 입력하세요."

    block1 = ImageBlock(
        image_url="http://www.pharm.or.kr/images/sb_photo/MFDS/big/201801310000501.jpg",
        alt_text="알약 이미지"
    )

    block2 = SectionBlock(
        text = keywords
    )

    if input1 == "선택":
        my_blocks = [block1, block2]
    else:
        my_blocks = [block2]

    slack_web_client.chat_postMessage(
        channel=channel,
        # text=keywords
        blocks=extract_json(my_blocks)
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
